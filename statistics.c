#include "statistics.h"
#include <math.h>
float desEst(int *arr)
{
	float media,standardDeviation = 0.0;
	media=med(arr);
	int i;
	for(i = 0; i < 12; ++i)
        standardDeviation += pow(arr[i] - media, 2);

    return sqrt(standardDeviation / 12);
}
float med(int *arr)
{
	int i;
	float prom,sum=0;
	for(i = 0; i < 12; ++i)
    {
        sum +=(float) arr[i];
    }
    prom=sum/12;
    return prom;
}
int min(int *arr)
{
	int min=arr[0];
	int i;
	for(i = 1; i < 12; ++i)
    {
        if (min > arr[i])
        {
        	min=arr[i];
        }
    }
    return min;
}
int max(int *arr)
{
	int max=arr[0];
	int i;
	for(i = 1; i < 12; ++i)
    {
        if (max < arr[i])
        {
        	max=arr[i];
        }
    }
    return max;
}
