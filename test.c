#include <stdio.h>    // Standard input/output definitions
#include <stdlib.h>
#include <string.h>   // String function definitions
#include <unistd.h>   // para usleep()
#include <getopt.h>
#include <math.h>
#include <limits.h>
#include "statistics.h"
#include "arduino-serial-lib.h"

float calculateSD(float data[]);

void error(char* msg)
{
    fprintf(stderr, "%s\n",msg);
    exit(EXIT_FAILURE);
}

int main(int argc, char *argv[])
{
	int fd = -1;
	int baudrate = 9600;  // default

	fd = serialport_init("/dev/ttyACM0", baudrate);

	if( fd==-1 )
	{
		error("couldn't open port");
		return -1;
	}
	
	char temperature = 't';
	char humedad = 'h';
	int* temp = (int *)malloc(sizeof(int)) ;
	int* hum = (int *)malloc(sizeof(int)) ;
	int mayortemp=0;
	int menortemp=INT_MAX;
	int mayorhum=0;
	int menorhum=INT_MAX;
	int temperaturas [12];
	int humedades [12];
	serialport_flush(fd);
	while(1){
		for(int muestra =0; muestra<12;muestra++){
			int verificar =0;
			write(fd,&temperature,1);
			usleep(5000);
			while(verificar ==0){
			 verificar = read(fd,temp,1);
			}
			verificar =0;
			usleep(5000);
			write(fd,&humedad,1);
			usleep(5000);
			while(verificar ==0){
			 verificar = read(fd,hum,1);
			};
			verificar =0;
			temperaturas[muestra]= *temp;
			humedades[muestra]= *hum;
			printf("Temperatura: %d | Humedad: %d\n",*temp,*hum);
			sleep(5);
		}
		printf("\nEstadísticas del minuto\n");
		printf("\nHumedades:\n");
		printf("Media: %.2f\n",med(humedades));
		printf("Desviación Estándar: %.2f\n",desEst(humedades));
		printf("Mínima: %d\n",min(humedades));
		printf("Máxima: %d\n",max(humedades));
		printf("\nTemperaturas:\n");
		printf("Media: %.2f\n",med(temperaturas));
		printf("Desviación Estándar: %.2f\n",desEst(temperaturas));
		printf("Mínima: %d\n",min(temperaturas));
		printf("Máxima: %d\n",max(temperaturas));

	}
	return 0;	
}


/* Ejemplo para calcular desviacion estandar y media */
float calculateSD(float data[])
{
    float sum = 0.0, mean, standardDeviation = 0.0;

    int i;

    for(i = 0; i < 10; ++i)
    {
        sum += data[i];
    }

    mean = sum/10;

    for(i = 0; i < 10; ++i)
        standardDeviation += pow(data[i] - mean, 2);

    return sqrt(standardDeviation / 10);
}

